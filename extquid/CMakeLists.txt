# external-quiddity

set(PLUGIN_NAME "extquid")
set(PLUGIN_DESCRIPTION "Example external quiddity")

option(PLUGIN_EXTQUID "${PLUGIN_DESCRIPTION}" ON)
add_feature_info("${PLUGIN_NAME}" PLUGIN_EXTQUID "${PLUGIN_DESCRIPTION}")

if (PLUGIN_EXTQUID)

    # extquid

    add_library(extquid SHARED
        extquid.cpp
        )

    # TEST
    
    add_executable(check_extquid_plugin check_extquid_plugin.cpp)
    add_test(check_extquid_plugin check_extquid_plugin)

    # INSTALL

    install(TARGETS
      extquid
      LIBRARY DESTINATION ${SWITCHER_LIBRARY}/plugins
      )

endif ()
