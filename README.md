Switcher external quiddity example
========

Standalone Switcher plugin example.

Provides a starting point to help build a plugin external to Switcher source tree.

Based on gst-quid example by Hantz-Carly F. Vius.

## Getting Started

```sh
cmake -B build -DCMAKE_BUILD_TYPE=Debug
make -sC build -j$(nproc)
sudo make install -sC build
```

Final plugin binary should be installed here `/usr/local/switcher-3.5/plugins/libextquid.so`.

Test plugin by running test.

```sh
build/extquid/check_extquid_plugin
```